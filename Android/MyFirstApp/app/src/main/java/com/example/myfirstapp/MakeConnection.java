package com.example.myfirstapp;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by ong on 1/5/2018.
 */

public class MakeConnection extends AsyncTask<Void, Void, Boolean>{
    private String ipAddress;
    private int port;
    public static Socket piSocket;

    MakeConnection(String ipAddress, int port){
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getIpAddress(){
        return this.ipAddress;
    }
    public int getPort(){
        return this.port;
    }
    public static synchronized Socket getPiSocket(){
        return piSocket;
    }

    public static synchronized void closePiSocket(){
        try {
            piSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Socket connSocket() {
        Socket socket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress(this.ipAddress, this.port);
        int timeoutInMs = 10*1000;   // 10 seconds
        try {
            socket.connect(socketAddress, timeoutInMs);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            try {
                socket.close();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return socket;
    }

    @Override
    protected Boolean doInBackground(Void... arg0) {
        Socket socket = this.connSocket();
        if (socket.isConnected()){
            MakeConnection.piSocket = socket;
            return true;
        } else {
            MakeConnection.piSocket = null;
            return false;
        }
    }

//    try {
//        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//        bufferedWriter.write("Testing 123");
//        bufferedWriter.flush();
//
//        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//
//        br.read(message);
//
//    } catch (IOException e) {
//        // TODO Auto-generated catch block
//        e.printStackTrace();
//    } finally {
//        try {
//            socket.close();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
}
