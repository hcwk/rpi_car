package com.example.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.net.Socket;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        Boolean isConnected = intent.getBooleanExtra(MainActivity.EXTRA_IS_CONNECTED, false);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView);
        if (isConnected){
            Socket piSocket = MakeConnection.getPiSocket();
            textView.setText(piSocket.getInetAddress().toString());
        } else{
            textView.setText("Not connected");
        }
    }

    public void onBackPressed()
    {
        MakeConnection.closePiSocket();
        super.onBackPressed();  // optional depending on your needs
    }

    public void closeConnection(View view) {
        MakeConnection.closePiSocket();
        super.onBackPressed();
    }

    public void startSimulation(View view) {
        Intent intent = new Intent(this, StartSimulation.class);
        startActivity(intent);
    }
}
