package com.example.myfirstapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_IS_CONNECTED = "com.example.myfirstapp.isConnected";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void makeConnection(View view){

        EditText editIp = (EditText) findViewById(R.id.editIp);
        EditText editPort = (EditText) findViewById(R.id.editPort);
        String ipAddress = editIp.getText().toString();
        String portStr = editPort.getText().toString();
        int port = Integer.parseInt(portStr);

        MakeConnection connection = new MakeConnection(ipAddress, port);
        try {
            Boolean isConnected = connection.execute().get();
            if (isConnected){
                Intent intent = new Intent(this, DisplayMessageActivity.class);
                intent.putExtra(EXTRA_IS_CONNECTED, true);
                startActivity(intent);
            } else{
                // change displaymessageactivity to display when not connected, and create another activity for real connection
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Connection refused. Please try again");
                alertDialogBuilder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

//        Boolean isConnected = false;
//        try {
//            isConnected = new MakeConnection(ipAddress, port).execute().get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//        intent.putExtra(EXTRA_IS_CONNECTED, isConnected);

    }

}
