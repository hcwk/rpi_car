import pygame
import RPi.GPIO as gpio
import time
import sys

def init():
    gpio.setmode(gpio.BCM)
    gpio.setup(17, gpio.OUT)
    gpio.setup(8, gpio.OUT)
    gpio.setup(27, gpio.OUT)
    gpio.setup(25, gpio.OUT)

def backward():
    init()
    gpio.output(17, True)
    gpio.output(27, False)
    gpio.output(8, True)
    gpio.output(25, False)
    
def forward():
    init()
    gpio.output(17, False)
    gpio.output(27, True)
    gpio.output(8, False)
    gpio.output(25, True)
    
def right():
    init()
    gpio.output(17, False)
    gpio.output(27, True)
    gpio.output(8, True)
    gpio.output(25, False)
    
def left():
    init()
    gpio.output(8, False)
    gpio.output(25, True)
    gpio.output(17, True)
    gpio.output(27, False)
    
leftmost = 12.5
neutral = 7.5
rightmost = 2.5
up = 12.5
down = 2.5

x_angle = neutral
y_angle = neutral

    
def left_right(current_angle, angle):
    if angle <= 12.5 and angle >=2.5:
        gpio.setmode(gpio.BCM)
        gpio.setup(2, gpio.OUT)
        x = gpio.PWM(2,50)
        x.start(current_angle)
        x.ChangeDutyCycle(angle)
        time.sleep(0.5)
        x.stop()
        gpio.cleanup(2)
    
def up_down(angle):
    if angle <= 12.5 and angle >=2.5:
        gpio.setmode(gpio.BCM)
        gpio.setup(3, gpio.OUT)
        y = gpio.PWM(3,50)
        y.start(angle)
        y.ChangeDutyCycle(angle)
        time.sleep(0.5)
        y.stop()
        gpio.cleanup(3)
        
def camera_init():
    gpio.setmode(gpio.BCM)
    gpio.setup(2, gpio.OUT)
    gpio.setup(3, gpio.OUT)
    x = gpio.PWM(2,50)
    y = gpio.PWM(3,50)
    x.start(neutral)
    y.start(neutral)
    while True:
        x.ChangeDutyCycle(neutral)
        y.ChangeDutyCycle(neutral)
        time.sleep(1)
        break
    x.stop()
    y.stop()
    gpio.cleanup()
        
pygame.init()
dimension = (500,400)
gameDisplay = pygame.display.set_mode(dimension)

camera_init()

quit = False
try:
    while not quit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit = True
            
            if event.type == pygame.KEYDOWN:
                pressed = pygame.key.get_pressed()
                
                if pressed[pygame.K_w]:
                    forward()
                if pressed[pygame.K_s]:
                    backward()
                if pressed[pygame.K_a]:
                    left()
                if pressed[pygame.K_d]:
                    right()
                    
                if pressed[pygame.K_LEFT]:
                    gpio.setmode(gpio.BCM)
                    gpio.setup(2, gpio.OUT)
                    x = gpio.PWM(2,50)
                    x.start(x_angle)
                    while not pygame.event.peek(pygame.KEYUP):
                        if x_angle + 0.25 <= leftmost:
                            x_angle += 0.25
                            x.ChangeDutyCycle(x_angle)
                        time.sleep(0.1)
                    x.stop()
                    gpio.cleanup(2)
                        
                if pressed[pygame.K_RIGHT]:
                    gpio.setmode(gpio.BCM)
                    gpio.setup(2, gpio.OUT)
                    x = gpio.PWM(2,50)
                    x.start(x_angle)
                    while not pygame.event.peek(pygame.KEYUP):
                        if x_angle - 0.25 >= rightmost:
                            x_angle -= 0.25
                            x.ChangeDutyCycle(x_angle)
                        time.sleep(0.1)
                    x.stop()
                    gpio.cleanup(2)
                    
                if pressed[pygame.K_UP]:
                    gpio.setmode(gpio.BCM)
                    gpio.setup(3, gpio.OUT)
                    y = gpio.PWM(3,50)
                    y.start(y_angle)
                    while not pygame.event.peek(pygame.KEYUP):
                        if y_angle + 0.25 <= up:
                            y_angle += 0.25
                            y.ChangeDutyCycle(y_angle)
                        time.sleep(0.1)
                    y.stop()
                    gpio.cleanup(3)
                    
                if pressed[pygame.K_DOWN]:
                    gpio.setmode(gpio.BCM)
                    gpio.setup(3, gpio.OUT)
                    y = gpio.PWM(3,50)
                    y.start(y_angle)
                    while not pygame.event.peek(pygame.KEYUP):
                        if y_angle - 0.25 >= down:
                            y_angle -= 0.25
                            y.ChangeDutyCycle(y_angle)
                        time.sleep(0.1)
                    y.stop()
                    gpio.cleanup(3)
                    
            if event.type == pygame.KEYUP:
                gpio.cleanup()
            
    
    gpio.cleanup()
    pygame.quit()
except:
    gpio.cleanup()
    print("Error: ", sys.exc_info()[0])
    pygame.quit()

