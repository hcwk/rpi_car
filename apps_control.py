import socket
import sys
import RPi.GPIO as gpio
import time
import threading
import queue

leftmost = 12.5
neutral = 7.5
rightmost = 2.5
up = 12.5
down = 2.5

x_angle = neutral
y_angle = neutral

HOST = ''  
PORT = 8000

data_queue = queue.Queue()

class receiveDataThread(threading.Thread):
    def run(self):
        while True:
            data = conn.recv(1024)
            if not data:
                data_queue.put(b'stop')
                break
            data_queue.put(data)

def init():
    gpio.setmode(gpio.BCM)
    gpio.setup(17, gpio.OUT)
    gpio.setup(8, gpio.OUT)
    gpio.setup(27, gpio.OUT)
    gpio.setup(25, gpio.OUT)

def backward():
    init()
    gpio.output(17, True)
    gpio.output(27, False)
    gpio.output(8, True)
    gpio.output(25, False)
    
def forward():
    init()
    gpio.output(17, False)
    gpio.output(27, True)
    gpio.output(8, False)
    gpio.output(25, True)
    
def right():
    init()
    gpio.output(17, False)
    gpio.output(27, True)
    gpio.output(8, True)
    gpio.output(25, False)
    
def left():
    init()
    gpio.output(8, False)
    gpio.output(25, True)
    gpio.output(17, True)
    gpio.output(27, False)

def camera_init():
    gpio.setmode(gpio.BCM)
    gpio.setup(2, gpio.OUT)
    gpio.setup(3, gpio.OUT)
    x = gpio.PWM(2,50)
    y = gpio.PWM(3,50)
    x.start(neutral)
    y.start(neutral)
    while True:
        x.ChangeDutyCycle(neutral)
        y.ChangeDutyCycle(neutral)
        time.sleep(1)
        break
    x.stop()
    y.stop()
    gpio.cleanup()

def camera_up_down(direction):
    global x_angle, y_angle
    gpio.setmode(gpio.BCM)
    gpio.setup(3, gpio.OUT)
    y = gpio.PWM(3,50)
    y.start(y_angle)
    if direction == "up":
        while data_queue.empty():
            if y_angle + 0.25 <= up:
                y_angle += 0.25
                y.ChangeDutyCycle(y_angle)
            time.sleep(0.1)
    elif direction == "down":
        while data_queue.empty():
            if y_angle - 0.25 >= down:
                y_angle -= 0.25
                y.ChangeDutyCycle(y_angle)
            time.sleep(0.1)
    y.stop()
    gpio.cleanup(3)
    
def camera_left_right(direction):
    global x_angle, y_angle
    gpio.setmode(gpio.BCM)
    gpio.setup(2, gpio.OUT)
    x = gpio.PWM(2,50)
    x.start(x_angle)
    if direction == "left":
        while data_queue.empty():
            if x_angle + 0.25 <= leftmost:
                x_angle += 0.25
                x.ChangeDutyCycle(x_angle)
            time.sleep(0.1)
    elif direction == "right":
        while data_queue.empty():
            if x_angle - 0.25 >= rightmost:
                x_angle -= 0.25
                x.ChangeDutyCycle(x_angle)
            time.sleep(0.1)
    x.stop()
    gpio.cleanup(2)
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setblocking(0)
print('# Socket created')
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print('# Bind failed. ', msg)
    sys.exit()
     
print('# Socket bind complete')
# Receive data from client
try:
    while True:
        # Start listening on socket
        s.listen(10)
        print('# Socket now listening')
         
        # Wait for client
        conn, addr = s.accept()
        print('# Connected to ' + addr[0] + ':' + str(addr[1]))
        
        camera_init()
        
        rdthread = receiveDataThread()
        rdthread.start()
        
        while True:     
            data = data_queue.get()
            line = data.decode('UTF-8')    # convert to string (Python 3 only)
            line = line.replace("\n","")   # remove newline character
            print( line )
            if line == "stop":
                break
            elif line == "car_forward_down":
                forward()
            elif line == "car_backward_down":
                backward()
            elif line == "car_left_down":
                left()
            elif line == "car_right_down":
                right()
            elif line == "camera_up_down":
                camera_up_down("up")
            elif line == "camera_down_down":
                camera_up_down("down")
            elif line == "camera_left_down":
                camera_left_right("left")
            elif line == "camera_right_down":
                camera_left_right("right")
            elif (line == "car_forward_up" or line == "car_backward_up" or line == "car_left_up" or line == "car_right_up"
                  or line == "camera_up"):
                gpio.cleanup()
                
        print("Disconnected. Making new connection...")
except:
    gpio.cleanup()
    s.close()
    print("Error: ", sys.exc_info()[0])
    raise
    
gpio.cleanup()
s.close()